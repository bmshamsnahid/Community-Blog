let Blog = require('../model/blog');

const getBlogs = (req, res, next) => {
    Blog.find({isPublished: true}, (err, blogs) => {
        if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
        return res.status(201).json({ success: true, data: blogs });
    });
};

const getBlog = (req, res, next) => {
    const blogId = req.params.blogId;
    if (!blogId) return res.status(201).json({success: false, message: 'Incomplete or invalid information.'});
    Blog.findById(blogId, (err, blog) => {
        if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
        else if (blog.isPublished == false) return res.status(201).json({ success: false, message: 'Blog ownership conflict.' });
        else return res.status(201).json({ success: true, data: blog });
    });
};

module.exports = {
    getBlogs,
    getBlog
}