let Comment = require('../model/comment'),
    Blog = require('../model/blog');

const createComment = (req, res, next) => {
    const blogId = req.params.blogId,
        commenterName = req.body.commenterName,
        commentBody = req.body.commentBody,
        commentDate = Date.now();
    if (!blogId || !commenterName || !commentBody) return res.status(201).json({success: false, message: 'Incomplete or invalid information.'});
    else {
        const comment = new Comment({ blogId: blogId, commenterName: commenterName, commentBody: commentBody, commentDate: commentDate });
        comment.save((err, comment) => {
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            else return res.status(201).json({ success: true, data: comment });
        });
    }
};

const getAllComments = (req, res, next) => {
    const blogId = req.params.blogId;
    if (!blogId) return res.status(201).json({success: false, message: 'Incomplete or invalid information.'});
    else {
        Comment.find({blogId: blogId}, (err, comments) => {
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            else return res.status(201).json({ success: true, data: comments });
        });
    }
};

const getComment = (req, res, next) => {
    const blogId = req.params.blogId;
    const commentId = req.params.commentId;
    if (!commentId) return res.status(201).json({success: false, message: 'Incomplete or invalid information.'});
    Comment.findById(commentId, (err, comment) => {
        if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
        else if (!comment) return res.status(201).json({ success: false, message: 'Comment does not exist' });
        else return res.status(201).json({ success: true, date: comment });
    });
};

const updateComment = (req, res, next) => {};

const deleteComment = (req, res, next) => {
    const bloggerId = req.params.bloggerId;
    const blogId = req.params.blogId;
    const commentId = req.params.commentId;
    if (!commentId || !blogId || !bloggerId) return res.status(201).json({success: false, message: 'Incomplete or invalid information.'});
    Blog.findById(blogId, (err, blog) => {
        if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
        else if (!blog) return res.status(201).json({ success: false, message: 'Existence conflict of blog.' });
        else {
            Comment.findById(commentId, (err, comment) => {
                if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
                else if (!comment) return res.status(201).json({ success: false, message: 'Existence conflict of comment.' });
                else if (blog.bloggerId != bloggerId || comment.blogId != blogId) return res.status(201).json({ success: false, message: 'Comment ownership conflict.' });
                else {
                    Comment.findByIdAndRemove(commentId, (err, comment) => {
                        if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
                        else return res.status(201).json({ success: true, message: 'Successfully removed the comment' });
                    });
                }
            });
        }
    });
};

module.exports = {
    createComment,
    getAllComments,
    getComment,
    updateComment,
    deleteComment
};