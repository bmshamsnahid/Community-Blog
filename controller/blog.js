let Blog = require('../model/blog');

const createBlog = (req, res) => {
    const blogTitle = req.body.blogTitle,
        blogSubtitle = req.body.blogSubtitle,
        blogKeyword = req.body.blogKeyword || [],
        blogBody = req.body.blogBody,
        blogEditDate = [Date.now()],
        blogPublishDate = req.body.blogPublishDate || Date.now(),
        isPublished = (typeof req.body.isPublished == 'undefined' || req.body.isPublished == false) ? false : true,
        bloggerId = req.params.bloggerId;
    if (!blogTitle || !blogSubtitle || !blogBody || !bloggerId) return res.status(201).json({success: false, message: 'Incomplete or invalid information.'});
    else {
        const newBlog = Blog({ blogTitle: blogTitle, blogSubtitle: blogSubtitle, blogKeyword: blogKeyword,
            blogBody: blogBody, blogEditDate: blogEditDate, blogPublishDate: blogPublishDate, isPublished: isPublished, bloggerId: bloggerId });
        newBlog.save((err, blog) => {
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            return res.status(201).json({ success: true, data: blog });
        });
    }
};

const getAllBlogs = (req, res) => {
    const bloggerId = req.params.bloggerId;
    Blog.find({bloggerId: bloggerId}, (err, blogs) => {
        if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
        return res.status(201).json({ success: true, data: blogs });
    });
};

const getBlog = (req, res) => {
    const blogId = req.params.blogId;
    const bloggerId = req.params.bloggerId;
    if (!blogId) {
        return res.status(201).json({success: false, message: 'Incomplete or invalid information or parameter.'});
    } else {
        Blog.findById(blogId, (err, blog) => {
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            else if (!blog) return res.status(400).json({ success: false, message: 'No blog found.' });
            else if (blog.isPublished || blog.bloggerId == bloggerId) return res.status(201).json({ success: true, data: blog });
            else return res.status(201).json({ success: false, message: 'Blog is not published yet or you are not the owner of the blog.' });
        });
    }
};

const updateBlog = (req, res) => {
    const blogTitle = req.body.blogTitle,
        blogSubtitle = req.body.blogSubtitle,
        blogKeyword = req.body.blogKeyword,
        blogBody = req.body.blogBody,
        blogPublishDate = req.body.blogPublishDate,
        isPublished = req.body.isPublished,
        blogId = req.params.blogId,
        bloggerId = req.params.bloggerId;
    if (!blogTitle || !blogSubtitle || typeof blogKeyword == 'undefined' ||
        !blogBody || typeof blogPublishDate == 'undefined' ||
        typeof isPublished == 'undefined' || !blogId || !bloggerId) return res.status(201).json({success: false, message: 'Incomplete or invalid information.'});
    else {
        Blog.findById(blogId, (err, blog) => {
            console.log('Update arena');
            console.log('Blogger Id received: ' + bloggerId);
            console.log('Blog.bloggerId: ' + blog.bloggerId);
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            else if (blog.bloggerId != bloggerId) return res.status(201).json({ success: false, message: 'You are not the owner of the blog.',
                bloggerId: bloggerId, blogBloggerID: blog.bloggerId });
            const currentDate = Date.now();
            blog.blogTitle = blogTitle;
            blog.blogSubtitle = blogTitle;
            blog.blogKeyword = blogKeyword;
            blog.blogBody = blogBody;
            blog.blogEditDate.push(currentDate);
            blog.blogPublishDate = blogPublishDate;
            blog.isPublished = isPublished;
            blog.save((err, blog) => {
                if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
                return res.status(201).json({ success: true, data: blog });
            });
        });
    }
};

const deleteBlog = (req, res) => {
    const blogId = req.params.blogId;
    const bloggerId = req.params.bloggerId;
    if (!blogId) {
        return res.status(201).json({success: false, message: 'Incomplete or invalid information or parameter.'});
    } else {
        Blog.findById(blogId, (err, blog) => {
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            else if (!blog) return res.status(400).json({ success: false, message: 'No blog found.' });
            else if (blog.bloggerId == bloggerId) {
                Blog.findByIdAndRemove(blogId, (err, blog) => {
                    if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
                    return res.status(201).json({ success: true, message: 'Removed the blog.' });
                });
            }
            else return res.status(201).json({ success: false, message: 'You are not the owner of the blog.' });
        });
    }
};

module.exports = {
    createBlog,
    getAllBlogs,
    getBlog,
    updateBlog,
    deleteBlog
};
