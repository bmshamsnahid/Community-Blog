var jwt = require('jsonwebtoken'),
    config = require('../config'),
    Blogger = require('../model/blogger');

var bloggerLogin = (req, res) => {
    var bloggerEmail = req.body.bloggerEmail,
        bloggerPassword = req.body.bloggerPassword;
    if (!bloggerEmail || !bloggerPassword)
        return res.status(201).json({success: false, message: 'Incomplete or invalid information.'});
    Blogger.findOne({bloggerEmail: bloggerEmail}, (err, blogger) => {
        if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
        if (!blogger) return res.status(201).json({ success: false, message: 'Blogger does not exist.' });
        blogger.comparePassword(bloggerPassword, (err, isMatch) => {
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            if (!isMatch) return res.status(201).json({ success: false, message: 'Invalid Blogger Credentials' });
            var token = jwt.sign(blogger, config.secret, { expiresIn: config.tokenexp });
            return res.status(201).json({ success: true, data: blogger, token: token });
        });
    });
};

var bloggerAuthenticate = (req, res, next) => {
    var token = req.body.token || req.query.token || req.headers['authorization'];
    if (token) {
        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            req.decoded = decoded;
            next();
        });
    } else {
        return res.status(201).json({ success: false, message: 'No token provided' });
    }
};

module.exports = {
    bloggerLogin,
    bloggerAuthenticate
};
