let Blogger = require('../model/blogger');

const createBlogger = (req, res, next) => {
    const bloggerName = req.body.bloggerName,
        bloggerEmail = req.body.bloggerEmail,
        bloggerPassword = req.body.bloggerPassword
    if (!bloggerName || !bloggerEmail || !bloggerPassword) return res.status(201).json({success: false, message: 'Incomplete or invalid information for blogger.'});
    else {
        Blogger.findOne({bloggerEmail: bloggerEmail}, (err, blogger) => {
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            if (blogger) return res.status(201).json({ success: false, message: 'Blogger already exists.', data: blogger });
            const newBlogger = new Blogger({ bloggerName: bloggerName, bloggerEmail: bloggerEmail, bloggerPassword: bloggerPassword });
            newBlogger.save((err, blogger) => {
                if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
                return res.status(201).json({ success: true, data: blogger });
            });
        });
    }
};

const getAllBloggers = (req, res, next) => {
    Blogger.find((err, bloggers) => {
        if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
        return res.status(201).json({ success: true, data: bloggers });
    });
};

const getBlogger = (req, res, next) => {
    var bloggerId = req.params.id;
    if (!bloggerId) {
        return res.status(201).json({success: false, message: 'Incomplete or invalid information or parameter.'});
    } else {
        Blogger.findById(bloggerId, (err, blogger) => {
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            if (!blogger) return res.status(400).json({ success: false, message: 'No blogger found.' });
            return res.status(201).json({ success: true, data: blogger });
        });
    }
};

const updateBlogger = (req, res, next) => {
    const bloggerName = req.body.bloggerName,
        bloggerEmail = req.body.bloggerEmail,
        bloggerId = req.params.id;
    if (!bloggerName || !bloggerEmail || !bloggerId) return res.status(201).json({success: false, message: 'Incomplete or invalid information.'});
    else {
        Blogger.findById(bloggerId, (err, blogger) => {
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            if (!blogger) return res.status(400).json({ success: false, message: 'No blogger Exist.' });
            blogger.bloggerName = bloggerName || blogger.bloggerName;
            blogger.bloggerEmail = bloggerEmail || blogger.bloggerEmail;
            blogger.save((err, blogger) => {
                if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
                return res.status(201).json({ success: true, data: blogger });
            });
        });
    }
};

const deleteBlogger = (req, res, next) => {
    var bloggerId = req.params.id;
    if (!bloggerId) {
        return res.status(201).json({success: false, message: 'Incomplete or invalid information or parameter.'});
    } else {
        Blogger.findByIdAndRemove(bloggerId, (err, blogger) => {
            if (err) return res.status(400).json({ success: false, message: 'Fatal server error: ' + err });
            return res.status(201).json({ success: true, message: 'Removed the blogger.' });
        });
    }
};

module.exports = {
    createBlogger,
    getAllBloggers,
    getBlogger,
    updateBlogger,
    deleteBlogger
};
