var express = require('express'),
    router = express.Router(),
    bloggerController = require('../controller/blogger'),
    authController = require('../controller/auth');

router.post('/', bloggerController.createBlogger);
router.get('/', bloggerController.getAllBloggers);
router.get('/:id', bloggerController.getBlogger);
router.patch('/:id', authController.bloggerAuthenticate, bloggerController.updateBlogger);
router.delete('/:id', authController.bloggerAuthenticate, bloggerController.deleteBlogger);

module.exports = router;