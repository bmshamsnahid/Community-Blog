var express = require('express'),
    router = express.Router(),
    commentController = require('../controller/comment'),
    authController = require('../controller/auth');

router.post('/:blogId', commentController.createComment);
router.get('/:blogId', commentController.getAllComments);
router.get('/:blogId/:commentId', commentController.getComment);
router.patch('/:blogId/:commentId', commentController.updateComment);
router.delete('/:bloggerId/:blogId/:commentId', authController.bloggerAuthenticate, commentController.deleteComment);

module.exports = router;