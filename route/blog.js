var express = require('express'),
    router = express.Router(),
    blogController = require('../controller/blog'),
    authController = require('../controller/auth');

router.post('/:bloggerId', authController.bloggerAuthenticate, blogController.createBlog);
router.get('/:bloggerId', authController.bloggerAuthenticate, blogController.getAllBlogs);
router.get('/:bloggerId/:blogId', authController.bloggerAuthenticate, blogController.getBlog);
router.patch('/:bloggerId/:blogId', authController.bloggerAuthenticate, blogController.updateBlog);
router.delete('/:bloggerId/:blogId', authController.bloggerAuthenticate, blogController.deleteBlog);

module.exports = router;