const express = require('express'),
    router = express.Router(),
    authController = require('../controller/auth');

router.post('/login', authController.bloggerLogin);

module.exports = router;