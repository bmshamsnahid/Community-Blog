let express = require('express'),
    router = express.Router(),
    publicController = require('../controller/public');

router.get('/blog/:blogId', publicController.getBlog);
router.get('/blog', publicController.getBlogs);

module.exports = router;