var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    mongoose = require('mongoose'),
    cors = require('cors'),
    path = require('path');

var config = require('./config');

var bloggerRoutes = require('./route/blogger'),
    blogRoutes = require('./route/blog'),
    commentRoutes = require('./route/comment'),
    authRoutes = require('./route/auth'),
    publicRoutes = require('./route/public');

var port = process.env.PORT || config.serverPort;

app.use(express.static(path.join(__dirname, 'public/dist')));

mongoose.Promise = global.Promise;
mongoose.connect(config.database, (err) => {
    if (err) {
        console.log('Error in database connection. ' + err);
    } else {
        console.log('Database connected');
    }
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(require('body-parser').json('*/*'));

app.use(morgan('dev'));

app.use(cors());

app.use('/blogger', bloggerRoutes);
app.use('/blog', blogRoutes);
app.use('/comment', commentRoutes);
app.use('/auth', authRoutes);
app.use('/public', publicRoutes);

app.listen(port);
console.log('App is running in port: ' + port);
