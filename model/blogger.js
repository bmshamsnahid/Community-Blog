var mongoose = require('mongoose'),
    bcrypt = require('bcrypt-nodejs'),
    Schema = mongoose.Schema;

const BloggerSchema = new Schema({
    bloggerName: { type: String, required: true },
    bloggerEmail: { type: String, required: true },
    bloggerPassword: { type: String, required: true },
    bloggerBlogId: [String]
});

// Pre-save of user's hash bloggerPassword to database
BloggerSchema.pre('save', function (next) {
    const users = this,
        SALT_FACTOR = 5;

    if (!users.isModified('bloggerPassword')) return next();

    bcrypt.genSalt(SALT_FACTOR, (err, salt) => {
        if (err) return next(err);

        bcrypt.hash(users.bloggerPassword, salt, null, (err, hash) => {
            if (err) return next(err);
            users.bloggerPassword = hash;
            next();
        });
    });
});

// Method to compare bloggerPassword for login
BloggerSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.bloggerPassword, (err, isMatch) => {
        if (err) { return cb(err); }

        cb(null, isMatch);
    });
};

module.exports = mongoose.model('Blogger', BloggerSchema, 'Blogger');
