var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Comment = require('./comment');

var BlogSchema = new Schema({
    blogTitle: { type: String, required: true },
    blogSubtitle: { type: String, required: true },
    blogKeyword: [String],
    blogBody: { type: String, required: true },
    blogEditDate: { type: [Date], required: false },
    blogPublishDate: { type: Date, required: false },
    isPublished: { type: Boolean, required: true, default: false },
    bloggerId: { type: String, required: true }
});

module.exports = mongoose.model('Blog', BlogSchema, 'Blog');