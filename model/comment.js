var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CommentSchema = new Schema({
    blogId: { type: String, required: true },
    commenterName: { type: String, required: true },
    commentBody: { type: String, required: true },
    commentDate: { type: Date, required: true, default: Date.now }
});

module.exports = mongoose.model('Comment', CommentSchema, 'Comment');