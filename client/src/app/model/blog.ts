import {BlogComment} from './comment';

export class Blog {
  _id: string;
  blogTitle: string;
  blogSubtitle: string;
  blogKeyword: string[];
  blogBody: string;
  blogEditedDate: Date[];
  blogPublishDate: Date;
  isPublished: boolean;
  bloggerId: string;
}
