export class BlogComment {
  _id: string;
  commenterName: string;
  commentBody: string;
  commentDate: Date;
  blogId: string;
}
