import {Blog} from './blog';

export class Blogger {
  _id: string;
  bloggerName: string;
  bloggerEmail: string;
  bloggerPassword: string;
  bloggerBlogId: string[];
}
