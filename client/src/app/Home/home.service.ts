import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class HomeService {
  getAllBlogUrl = 'http://localhost:8080/public/blog';

  constructor(private http: Http) {}

  getBlogs() {
    const headers = new Headers();
    const options = new RequestOptions({headers: headers})
    return this.http.get(this.getAllBlogUrl, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error?: Response): any {
    if (error) {
      console.log(error);
    }
  }
}
