import {Component, OnInit} from '@angular/core';
import {HomeService} from './home.service';
import {Blog} from '../model/blog';
import {ToastrService} from '../common/toastr.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: [
    './home.component.css'
  ]
})
export class HomeComponent implements OnInit {

  blogs: Blog[];

  constructor(private homeService: HomeService,
                private toastrService: ToastrService) {}
  ngOnInit() {
    this.homeService.getBlogs().subscribe((data) => {
      if (data.success === true) {
        this.blogs = data.data;
      } else {
        if (data.message) {
          this.toastrService.warning(data.message);
        } else {
          this.toastrService.error('Error in fetching public blog posts');
        }
      }
    });
  }
}
