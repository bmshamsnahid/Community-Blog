import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {BlogComment} from "../model/comment";

@Injectable()
export class PostServiceService {
  jwtToken: string;
  getBlogUrl = '';
  constructor(private http: Http) {
    const userOb = JSON.parse(localStorage.getItem('currentBlogger'));
    if (userOb) {
      this.jwtToken = userOb.token;
    }
  }

  getBlog(blogId: any) {
    const headers = new Headers();
    const options = new RequestOptions({headers: headers});
    return this.http.get(`http://localhost:8080/public/blog/${blogId}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  getComments(blogId) {
    const headers = new Headers();
    const options = new RequestOptions({headers: headers});
    return this.http.get(`http://localhost:8080/comment/${blogId}`)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  createComment(blogComment: BlogComment, blogId: string) {
    const headers = new Headers();
    headers.append('Authorization', `${this.jwtToken}`);
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({headers: headers});
    return this.http.post(`http://localhost:8080/comment/${blogId}`, JSON.stringify(blogComment), options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error?: Response): any {
    if (error) {
      console.log(error);
    }
  }
}
