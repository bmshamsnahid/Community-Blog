import {Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {PostServiceService} from './post-service.service';
import {Blog} from '../model/blog';
import {ToastrService} from '../common/toastr.service';
import {DashboardService} from '../Dashboard/dashboard.service';
import {Blogger} from '../model/blogger';
import {BlogComment} from '../model/comment';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: [
    './post.component.css'
  ]
})
export class PostComponent implements OnInit {
  blogPost: Blog = new Blog();
  blogger: Blogger = new Blogger;
  comments: BlogComment[] = [];
  newComment: BlogComment;
  constructor(private activatedRoute: ActivatedRoute,
              private location: Location,
              private postService: PostServiceService,
              private toastrService: ToastrService,
              private dashboardService: DashboardService) {}
  ngOnInit() {
    this.newComment = new BlogComment();
    this.getBlog();
  }

  getBlog() {
    const blogId = this.activatedRoute.snapshot.params.blogId;
    this.postService.getBlog(blogId)
      .subscribe((blogData) => {
        if (blogData.success === true) {
          this.blogPost = blogData.data;
          this.dashboardService.getBloggerProfile(this.blogPost.bloggerId)
            .subscribe((bloggerData) => {
              if (bloggerData.success === true) {
                this.blogger = bloggerData.data;
                console.log(this.blogger);
              } else if (bloggerData.message) {
                this.toastrService.warning(blogData.message);
              } else {
                this.toastrService.error('Error in get the blog Post');
              }
          });
          this.postService.getComments(this.blogPost._id)
            .subscribe((commentsData) => {
              if (commentsData.success) {
                this.comments = commentsData.data;
                console.log(this.comments[0]);
              } else if (commentsData.message) {
                this.toastrService.warning(commentsData.message);
              } else {
                this.toastrService.error('Error in fetching the comments.');
              }
            });
        } else if (blogData.message) {
          this.toastrService.warning(blogData.message);
        } else {
          this.toastrService.error('Error in get the blog Post');
        }
    });
  }

  createComment() {
    if (!this.newComment.commentBody || !this.newComment.commenterName) {
      this.toastrService.warning('Invalid or incomplete information.');
    } else {
      console.log(this.newComment);
      this.postService.createComment(this.newComment, this.blogPost._id).subscribe((data) => {
        if (data.success) {
          this.comments.push(data.data);
          this.newComment = new BlogComment();
          this.toastrService.success('Successfully created the comment.');
        } else if (data.message) {
          this.toastrService.warning(data.message);
        } else {
          this.toastrService.error('Error in comment in the post');
        }
      });
    }
  }
}
