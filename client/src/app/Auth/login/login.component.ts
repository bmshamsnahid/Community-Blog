import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from '../../common/toastr.service';
import {AuthService} from '../../common/auth.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.css'
  ]
})
export class LoginComponent implements OnInit {

  bloggerEmail = new FormControl('', [Validators.email]);
  bloggerPassword = new FormControl('', [Validators.required]);

  bloggerLoginForm: FormGroup = this.formBuilder.group({
    bloggerEmail: this.bloggerEmail,
    bloggerPassword: this.bloggerPassword
  });

  constructor(private formBuilder: FormBuilder,
              private toastrService: ToastrService,
              private authService: AuthService,
              private router: Router) {}
  ngOnInit() {}

  loginBlogger(formData) {
    console.log(formData);
    this.authService.bloggerLogin(formData).subscribe((data) => {
      if (data.success) {
        this.toastrService.success('Successfully logged in as Blogger');
        this.router.navigate(['dashboard']);
      } else if (data.message) {
        this.toastrService.warning(data.message);
      } else {
        this.toastrService.error('Error in log in to the server.');
      }
    });
  }
}
