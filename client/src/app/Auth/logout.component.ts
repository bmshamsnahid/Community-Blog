import {Component, OnInit} from '@angular/core';
import {AuthService} from '../common/auth.service';
import {ToastrService} from '../common/toastr.service';
import {Router} from '@angular/router';

@Component({
  template: ``
})
export class LogoutComponent implements OnInit {
  constructor(private authService: AuthService,
              private toastrService: ToastrService,
              private router: Router) {}
  ngOnInit() {
    console.log('Logout component.');
    this.authService.bloggerLogout();
    this.toastrService.info('Successfully log out as Blogger.');
    this.router.navigate(['login']);
  }
}
