import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from '../../common/toastr.service';
import {AuthService} from '../../common/auth.service';
import {Blogger} from '../../model/blogger';

@Component({
  templateUrl: './register.component.html',
  styleUrls: [
    './register.component.css'
  ]
})
export class RegisterComponent implements OnInit {
  newBlogger: Blogger;
  bloggerEmail = new FormControl('', [Validators.email]);
  bloggerPassword = new FormControl('', [Validators.required]);
  bloggerName = new FormControl('', [Validators.required]);

  bloggerRegistrationForm: FormGroup = this.formBuilder.group({
    bloggerEmail: this.bloggerEmail,
    bloggerPassword: this.bloggerPassword,
    bloggerName: this.bloggerName
  });

  constructor(private formBuilder: FormBuilder,
              private toastrService: ToastrService,
              private authService: AuthService,
              private router: Router) {}
  ngOnInit() {
    this.newBlogger = new Blogger();
  }

  registerBlogger(formData) {
    // console.log(formData);
    this.authService.bloggerRegister(formData).subscribe((data) => {
      // console.log(data);
      if (data.success) {
        this.toastrService.success('Successfully created the account');
        this.router.navigate(['/login']);
      } else if (data.message) {
        this.toastrService.warning(data.message);
      } else {
        this.toastrService.error('Error in creating new blogger account');
      }
    });
  }
}
