import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {LoginComponent} from './Auth/login/login.component';
import {RegisterComponent} from './Auth/register/register.component';
import {DashboardComponent} from './Dashboard/dashboard.component';
import {HomeComponent} from './Home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {AuthService} from './common/auth.service';
import {AuthGuardService} from './common/auth-guard.service';
import {ToastrService} from './common/toastr.service';
import {DashboardService} from './Dashboard/dashboard.service';
import {HomeService} from './Home/home.service';
import {NavbarComponent} from './navbar/navbar.component';
import {FooterComponent} from './footer/footer.component';
import {PostComponent} from './post/post.component';
import {PostServiceService} from './post/post-service.service';
import {LogoutComponent} from './Auth/logout.component';
import {ProfileComponent} from './profile/profile.component';
import { CKEditorModule } from 'ng2-ckeditor';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    DashboardComponent,
    HomeComponent,
    NavbarComponent,
    FooterComponent,
    PostComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CKEditorModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent, pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'logout', component: LogoutComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'post/:blogId', component: PostComponent },
      { path: '**', component: HomeComponent, pathMatch: 'full' }
    ])
  ],
  providers: [
    AuthService,
    AuthGuardService,
    ToastrService,
    DashboardService,
    HomeService,
    PostServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
