import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Blog} from "../model/blog";
import {BlogComment} from "../model/comment";

@Injectable()
export class DashboardService {
  jwtToken: string;

  constructor(private http: Http) {
    const userOb = JSON.parse(localStorage.getItem('currentBlogger'));
    if (userOb) {
      this.jwtToken = userOb.token;
    }
  }

  getBloggerProfile(bloggerId) {
    const headers = new Headers({'Content-type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    return this.http.get(`http://localhost:8080/blogger/${bloggerId}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  getBloggerPosts(bloggerId) {
    const headers = new Headers();
    headers.append('Authorization', `${this.jwtToken}`);
    const options = new RequestOptions({headers: headers});
    return this.http.get(`http://localhost:8080/blog/${bloggerId}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  updateBlogPost(blog: Blog, bloggerId: string) {
    const headers = new Headers();
    headers.append('Authorization', `${this.jwtToken}`);
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({headers: headers});
    return this.http.patch(`http://localhost:8080/blog/${bloggerId}/${blog._id}`, JSON.stringify(blog), options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  deleteBlogPost(blog: Blog, bloggerId: string) {
    const headers = new Headers();
    headers.append('Authorization', `${this.jwtToken}`);
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({headers: headers});
    return this.http.delete(`http://localhost:8080/blog/${bloggerId}/${blog._id}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  createBlogPost(blog: Blog, bloggerId: string) {
    const headers = new Headers();
    headers.append('Authorization', `${this.jwtToken}`);
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({headers: headers});
    return this.http.post(`http://localhost:8080/blog/${bloggerId}`, JSON.stringify(blog), options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  deleteComment(bloggerId: string, blogId: string, blogComment: BlogComment) {
    const headers = new Headers();
    headers.append('Authorization', `${this.jwtToken}`);
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({headers: headers});
    return this.http.delete(`http://localhost:8080/comment/${bloggerId}/${blogId}/${blogComment._id}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error?: Response): any {
    if (error) {
      console.log(error);
    }
  }
}
