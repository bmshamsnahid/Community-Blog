import {Component, OnInit} from '@angular/core';
import {DashboardService} from './dashboard.service';
import {Blogger} from '../model/blogger';
import {Blog} from '../model/blog';
import {ToastrService} from '../common/toastr.service';
import {BlogComment} from '../model/comment';
import {PostServiceService} from '../post/post-service.service';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: [
    './dashboard.component.css'
  ]
})
export class DashboardComponent implements OnInit {
  jwtToken: string;
  currentBlogger: Blogger;
  blogPosts: Blog[];
  currentBlog: Blog;
  newBlog: Blog;
  blogComments: BlogComment[];
  selectedComment: BlogComment;
  constructor(private dashboardService: DashboardService,
              private toastrService: ToastrService,
              private postService: PostServiceService) {}
  ngOnInit() {
    this.currentBlogger = JSON.parse(localStorage.getItem('currentBlogger')).data;
    this.jwtToken = JSON.parse(localStorage.getItem('currentBlogger')).token;
    this.currentBlog = new Blog();
    this.newBlog = new Blog();
    this.blogComments = [];
    this.selectedComment = new BlogComment();

    this.dashboardService.getBloggerPosts(this.currentBlogger._id).subscribe((data) => {
      if (data.success) {
        this.blogPosts = data.data;
        console.log(this.blogPosts);
      } else if (data.message) {
        this.toastrService.warning(data.message);
      } else {
        this.toastrService.error('Error in fetch data');
      }
    });
  }

  onClickTableBlog(blog: Blog) {
    this.currentBlog = blog;
    console.log(this.currentBlog);
  }

  onClickUpdateBlog() {
    console.log(this.currentBlog);
    this.dashboardService.updateBlogPost(this.currentBlog, this.currentBlogger._id).subscribe((data) => {
      console.log(data);
      if (data.success) {
        this.currentBlog = data.data;
        this.toastrService.success('Successfully updated the blog');
      } else if (data.message) {
        this.toastrService.warning(data.message);
      } else {
        this.toastrService.error('Error in updating the blog');
      }
    });
  }
  onClickDeleteBlog() {
    console.log(this.currentBlog);
    this.dashboardService.deleteBlogPost(this.currentBlog, this.currentBlogger._id).subscribe((data) => {
      console.log(data);
      if (data.success) {
        this.toastrService.success('Successfully removed the blog');
        this.blogPosts = this.blogPosts.filter(blog => blog._id !== this.currentBlog._id);
      } else if (data.message) {
        this.toastrService.warning(data.message);
      } else {
        this.toastrService.error('Error in deleting the blog Post');
      }
    });
  }
  onClickDraftBlog() {
    this.currentBlog.isPublished = false;
    this.dashboardService.updateBlogPost(this.currentBlog, this.currentBlogger._id).subscribe((data) => {
      console.log(data);
      if (data.success) {
        this.currentBlog = data.data;
        this.toastrService.success('Successfully drafted the blog');
      } else if (data.message) {
        this.toastrService.warning(data.message);
      } else {
        this.toastrService.error('Error in drafting the blog');
      }
    });
  }
  onClickPublishBlog() {
    this.currentBlog.isPublished = true;
    this.dashboardService.updateBlogPost(this.currentBlog, this.currentBlogger._id).subscribe((data) => {
      console.log(data);
      if (data.success) {
        this.currentBlog = data.data;
        this.toastrService.success('Successfully published the blog');
      } else if (data.message) {
        this.toastrService.warning(data.message);
      } else {
        this.toastrService.error('Error in publishing the blog');
      }
    });
  }
  onClickCreateBlog() {
    if (!this.newBlog.blogTitle || !this.newBlog.blogSubtitle || !this.newBlog.blogBody) {
      this.toastrService.warning('Invalid or incomplete blog information');
    } else {
      this.dashboardService.createBlogPost(this.newBlog, this.currentBlogger._id).subscribe((data) => {
          if (data.success) {
            this.blogPosts.push(data.data);
            this.newBlog = new Blog();
            this.toastrService.success('Successfully created the blog.');
          } else if (data.message) {
            this.toastrService.warning(data.message);
          } else {
            this.toastrService.error('Error in creating the post');
          }
      });
    }
  }
  onClickComments() {
    this.postService.getComments(this.currentBlog._id).subscribe((commentsData) => {
      if (commentsData.success) {
        this.blogComments = commentsData.data;
        console.log(this.blogComments);
      } else if (commentsData.message) {
        this.toastrService.warning(commentsData.message);
      } else {
        this.toastrService.error('Error in fetching the comments.');
      }
    });
  }
  onClickDeleteComment(blogComment: BlogComment) {
    console.log(blogComment);
    this.dashboardService.deleteComment(this.currentBlogger._id, this.currentBlog._id, blogComment).subscribe((data) => {
      if (data.success) {
        this.toastrService.success('Successfuly deleted the comment');
        this.blogComments = this.blogComments.filter(bc => bc._id !== blogComment._id);
      } else if (data.message) {
        this.toastrService.warning(data.message);
      } else {
        this.toastrService.error('Error in deleting the comment');
      }
    });

  }
}
